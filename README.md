# simpath [FR]

Ce paquetage `R` est une application [Shiny](https://shiny.rstudio.com/) qui peut être personnalisée pour la visualisation, l'analyse et la simulation de données financières.

## Installation

Pour installer la version à jour de `simpath` il suffit d'exécuter la commande suivante à partir de la console `R` :

``` r
remotes::install_gitlab("cacsfre/simpath")
```

## Exemple

Pour lancer l'application `shiny`, il y a deux options :

+ Lancer avec la fonction `simpath::run_simpath()`.
+ Lancer avec la fonction `shiny::runApp` à partir du dossier `./inst/app`.

## Personnaliser

Les [modules](https://shiny.rstudio.com/articles/modules.html) `shiny` peuvent être modifiés/ajoutés dans le dossier `./R`.

## demo

![demo](demo.gif)

# simpath [EN]

This `R` package is a [Shiny](https://shiny.rstudio.com/) application which can be customized to visualize, analyse and simulate financial data.

## Installation

You can install the current version of `simpath` with:

``` r
remotes::install_gitlab("cacsfre/simpath")
```

## Example

To launch the `shiny` app, you have two options:

+ Launch with the `simpath::run_simpath()` function.
+ Launch with the `shiny::runApp` function from the `inst/app` folder.


## Customize

You can add/remove `shiny` [modules](https://shiny.rstudio.com/articles/modules.html) through the `./R` folder.


## demo

![demo](demo.gif)
