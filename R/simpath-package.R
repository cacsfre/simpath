#' simpath
#'
#' A \code{shiny} application to generate random paths for financial simulations.
#'
#' @docType package
#' @name simpath
#' @import shiny
#' @import shinyMobile
#' @import plotly
#' @import openxlsx
#' @import DT
#' @import waiter
NULL
